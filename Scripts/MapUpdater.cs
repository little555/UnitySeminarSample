﻿using UnityEngine;

public class MapUpdater : MonoBehaviour {

    /** 道 */
    public GameObject green1;
    /** 道 */
    public GameObject green2;

    /** 道を作る閾値 */
    private float border = 1000f;

    /**
     * 更新処理
     * */
    void Update () {

        if (this.transform.position.z > border)
        {
            this.UpdateMap();
        }
    }

    /**
     * マップを更新
     * */
    private void UpdateMap()
    {
        if (this.green1.transform.position.z < this.border)
        {
            this.border += 2000;
            Vector3 temp = new Vector3(0, 0.05f, this.border);
            this.green1.transform.position = temp;
        }
        else if (this.green2.transform.position.z < this.border)
        {
            this.border += 2000;
            Vector3 temp = new Vector3(0, 0.05f, this.border);
            this.green2.transform.position = temp;
        }
    }
}
